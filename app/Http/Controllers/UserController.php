<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    //Create User
    public function create()
    {
        return view('users.register');
    }
    //create new user
    public function store(Request $request)
    {
        $validateUser=$request->validate([
            'name'=>['required','min:3'],
            'email'=>['required','email',Rule::unique('users','email')],
            'password'=>['required','confirmed','min:6'],
        ]);
        $validateUser['password']=bcrypt($validateUser['password']);
        $user=User::create($validateUser);

        //login
        auth()->login($user);

        return redirect('/')->with('message','User Created and Logged In');
    }
    //logout
    public function logout(Request $request)
    {
        auth()->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/')->with('message','You have been Logged out');

    }
    //login
    public function login()
    {
        return view('users.login');
    }
    //authenticate
    public function authenticate(Request $request)
    {
        $validateUser=$request->validate([
            'email'=>['required','email'],
            'password'=>'required'
        ]);

        if (auth()->attempt($validateUser)){
            $request->session()->regenerate();
            return redirect('/')->with('message','You are Now Logged In');
        }

        return back()->withErrors(['email' => 'Invalid Credentials'])->onlyInput('email');    }
}
