<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    use HasFactory;
    protected $fillable=[
        'user_id',
        'title',
        'logo',
        'tags',
        'company',
        'location',
        'email',
        'website',
        'description',
    ];
    public function scopeFilter($query,array $filters)
    {
        //Tag Filter
        if($filters['tag'] ?? false){
            $query->where('tags','like','%'.request('tag').'%');
        }
        //Search Filter
        if($filters['search'] ?? false){
            $query->where('title','like','%'.request('search').'%')
                ->orWhere('description','like','%'.request('search').'%')
                ->orWhere('tags','like','%'.request('search').'%');
        }
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
