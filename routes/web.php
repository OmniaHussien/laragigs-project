<?php

use App\Http\Controllers\ListingController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

//All Listings
Route::get('/',[ListingController::class,'index']);
//single listing
//route model binding
Route::get('/listing/{listing}', [ListingController::class,'show']);

//create a listing
Route::get('/listings/create',[ListingController::class,'create'])->middleware('auth');

//store Listings
Route::post('/listings',[ListingController::class,'store'])->middleware('auth');

//edit form listing
Route::get('/listings/{listing}/edit', [ListingController::class, 'edit'])->middleware('auth');

//update form listing
Route::put('/listings/{listing}', [ListingController::class,'update'])->middleware('auth');

//delete listings
Route::delete('/listings/{listing}', [ListingController::class,'destroy'])->middleware('auth');

//show register/create form
Route::get('/register',[UserController::class,'create'])->middleware('guest');

//create new user
Route::post('/users',[UserController::class,'store']);

//user login
Route::get('/login',[UserController::class,'login'])->name('login');

//login
Route::post('/users/authenticate',[UserController::class,'authenticate']);
//logout user
Route::post('/logout',[UserController::class,'logout'])->middleware('auth');

//manage listing
Route::get('/listings/manage',[ListingController::class,'manage'])->middleware('auth');
